﻿using Commons.BaseModel;
using Commons.ModelView;
using Data.Context;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Data.Entities;

namespace Repository.Implementation
{
    public class SkillAccess : ISkillAccess
    {
        private readonly SkillContext _context;

        public SkillAccess(SkillContext context)
        {
            _context = context;
        }
        public async Task<ResponseDto<List<SkillViewModel>>> GetAllSkill()
        {
            try
            {
                List<SkillViewModel> data = await (from a in _context.TblMSkills
                                                   select new SkillViewModel
                                                   {
                                                       SkillIdPk = a.SkillIdPk,
                                                       SkillName = a.SkillName
                                                   }).ToListAsync();

                return new ResponseDto<List<SkillViewModel>>(data);               
            }
            catch (Exception ex)
            {
                return new ResponseDto<List<SkillViewModel>>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillViewModel>> GetByIdSkill(int id)
        {
            try
            {
                SkillViewModel data = await(from a in _context.TblMSkills
                                           where a.SkillIdPk == id
                                           select new SkillViewModel
                                           {
                                               SkillIdPk = a.SkillIdPk,
                                               SkillName = a.SkillName
                                           }).FirstOrDefaultAsync();
                return new ResponseDto<SkillViewModel>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }

        public Task<ResponseDto<SkillViewModel>> PostCreateSkill(SkillViewModel data)
        {
            throw new NotImplementedException();
        }

        public async Task<ResponseDto<SkillViewModel>> PostEditSkill(SkillViewModel data)
        {
            try
            {
                TblMSkill a = new TblMSkill
                {
                    SkillIdPk = data.SkillIdPk,
                    SkillName = data.SkillName                   
                };
                _context.Update(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }
    }
}
