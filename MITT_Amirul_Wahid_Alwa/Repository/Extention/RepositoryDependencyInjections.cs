﻿using Microsoft.Extensions.DependencyInjection;
using Repository.Implementation;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Extention
{
    public static class RepositoryDependencyInjections
    {
        public static IServiceCollection AddRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<ISkillAccess, SkillAccess>();
            return services;
        }
    }
}
