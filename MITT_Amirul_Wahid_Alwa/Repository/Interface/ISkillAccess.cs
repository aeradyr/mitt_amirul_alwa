﻿using Commons.BaseModel;
using Commons.ModelView;
using Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interface
{
    public interface ISkillAccess
    {
        Task<ResponseDto<List<SkillViewModel>>> GetAllSkill();
        Task<ResponseDto<SkillViewModel>> GetByIdSkill(int id);
        Task<ResponseDto<SkillViewModel>> PostCreateSkill(SkillViewModel data);
        Task<ResponseDto<SkillViewModel>> PostEditSkill(SkillViewModel data);
    }
}
