﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Commons.ModelView
{
    public class SkillViewModel
    {
        public int SkillIdPk { get; set; }
        [DisplayName("Nama")]
        public string SkillName { get; set; }
    }
}
