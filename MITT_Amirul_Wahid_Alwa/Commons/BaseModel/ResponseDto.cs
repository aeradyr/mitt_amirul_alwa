﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.BaseModel
{
    public class ResponseDto<T>
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
        public ResponseDto()
        {
            IsSuccess = true;
            Message = string.Empty;
        }
        public ResponseDto(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            Message = message;
        }

        public ResponseDto(T data)
        {
            IsSuccess = true;
            Message = String.Empty;
            Data = data;
        }

        public ResponseDto(bool isSuccess, string message, T data)
        {
            IsSuccess=isSuccess;
            Message = message;
            Data = data;
        }
    }
}
