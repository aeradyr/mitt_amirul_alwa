﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Entities
{
    public partial class TblMSkillLevel
    {
        public int SkillLevelIdPk { get; set; }
        public string SkillLevelName { get; set; }
    }
}
