﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Entities
{
    public partial class TblMUser
    {
        public string UserNamePk { get; set; }
        public string Password { get; set; }
    }
}
