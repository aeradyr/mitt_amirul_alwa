﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Entities
{
    public partial class TblMSkill
    {
        public int SkillIdPk { get; set; }
        public string SkillName { get; set; }
    }
}
