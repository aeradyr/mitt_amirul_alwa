﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Entities
{
    public partial class TblMUserProfile
    {
        public string UserNameFk { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime? Bod { get; set; }
        public string Email { get; set; }
    }
}
