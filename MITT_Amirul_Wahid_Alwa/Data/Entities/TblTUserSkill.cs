﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Entities
{
    public partial class TblTUserSkill
    {
        public string UserSkillIdPk { get; set; }
        public string UserNameFk { get; set; }
        public string SkillIdFk { get; set; }
        public string SkillLevelIdFk { get; set; }
    }
}
