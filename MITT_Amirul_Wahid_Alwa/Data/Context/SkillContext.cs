﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Data.Entities;

#nullable disable

namespace Data.Context
{
    public partial class SkillContext : DbContext
    {
        public SkillContext()
        {
        }

        public SkillContext(DbContextOptions<SkillContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblMSkill> TblMSkills { get; set; }
        public virtual DbSet<TblMSkillLevel> TblMSkillLevels { get; set; }
        public virtual DbSet<TblMUser> TblMUsers { get; set; }
        public virtual DbSet<TblMUserProfile> TblMUserProfiles { get; set; }
        public virtual DbSet<TblTUserSkill> TblTUserSkills { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=localhost; initial catalog=db_user_skill; user id=sa; password=Lauragiga5; MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Japanese_CI_AS");

            modelBuilder.Entity<TblMSkill>(entity =>
            {
                entity.HasKey(e => e.SkillIdPk)
                    .HasName("PK__tbl_m_sk__78EFC431DBBDB32D");

                entity.ToTable("tbl_m_skill");

                entity.Property(e => e.SkillIdPk)
                    .ValueGeneratedNever()
                    .HasColumnName("skill_id_pk");

                entity.Property(e => e.SkillName)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("skill_name");
            });

            modelBuilder.Entity<TblMSkillLevel>(entity =>
            {
                entity.HasKey(e => e.SkillLevelIdPk)
                    .HasName("PK__tbl_m_sk__85ED4529E71FBC9A");

                entity.ToTable("tbl_m_skill_level");

                entity.Property(e => e.SkillLevelIdPk)
                    .ValueGeneratedNever()
                    .HasColumnName("skill_level_id_pk");

                entity.Property(e => e.SkillLevelName)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("skill_level_name");
            });

            modelBuilder.Entity<TblMUser>(entity =>
            {
                entity.HasKey(e => e.UserNamePk)
                    .HasName("PK__tbl_m_us__7266C343029EAA6C");

                entity.ToTable("tbl_m_user");

                entity.Property(e => e.UserNamePk)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("user_name_pk");

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("password");
            });

            modelBuilder.Entity<TblMUserProfile>(entity =>
            {
                entity.HasKey(e => e.UserNameFk)
                    .HasName("PK__tbl_m_us__7259D69461E5A5CE");

                entity.ToTable("tbl_m_user_profile");

                entity.Property(e => e.UserNameFk)
                    .HasMaxLength(50)
                    .HasColumnName("user_name_fk");

                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .HasColumnName("address");

                entity.Property(e => e.Bod)
                    .HasColumnType("date")
                    .HasColumnName("bod");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<TblTUserSkill>(entity =>
            {
                entity.HasKey(e => e.UserSkillIdPk)
                    .HasName("PK__tbl_t_us__4D3A02FA84626AE5");

                entity.ToTable("tbl_t_user_skill");

                entity.Property(e => e.UserSkillIdPk)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("user_skill_id_pk");

                entity.Property(e => e.SkillIdFk)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("skill_id_fk");

                entity.Property(e => e.SkillLevelIdFk)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("skill_level_id_fk");

                entity.Property(e => e.UserNameFk)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("user_name_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
