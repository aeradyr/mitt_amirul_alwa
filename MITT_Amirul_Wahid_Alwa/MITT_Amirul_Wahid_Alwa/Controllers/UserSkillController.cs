﻿using Commons.ModelView;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MITT_Amirul_Wahid_Alwa.Controllers
{

    [ApiController]
    public class UserSkillController : Controller
    {
        private readonly ISkillAccess _skillAccess;

        public UserSkillController(ISkillAccess skillAccess)
        {
            _skillAccess = skillAccess;
        }

        [Route("api/GetSkills")]
        public async Task<IActionResult> Index()
        {
            var data = await _skillAccess.GetAllSkill();

            List<SkillViewModel> list = new List<SkillViewModel>();

            if (data.IsSuccess)
            {
                foreach (var item in data.Data)
                {
                    list.Add(item);
                }
            }

            return View(list);
        }

        [Route("api/GetSkills{id}")]
        public async Task<IActionResult> DetailSkill(int id)
        {
            var data = await _skillAccess.GetByIdSkill(id);

            SkillViewModel a = new SkillViewModel();
            if (data.IsSuccess && data.Data != null)
            {
                a.SkillIdPk = data.Data.SkillIdPk;
                a.SkillName = data.Data.SkillName;
            };
            return View(a);
        }

        [Route("api/UpdateGetSkill{id}")]
        public async Task<IActionResult> EditSkill(int id)
        {
            var data = await _skillAccess.GetByIdSkill(id);

            SkillViewModel a = new SkillViewModel();
            if (data.IsSuccess && data.Data != null)
            {
                a.SkillIdPk = data.Data.SkillIdPk;
                a.SkillName = data.Data.SkillName;


            };
            return View(a);
        }


        [HttpPost]
        [Route("api/UpdatePostSkill{id}")]
        public async Task<IActionResult> EditSkill(SkillViewModel a, int id)
        {
            await _skillAccess.PostEditSkill(a);
            return RedirectToAction("Index");
        }


    }
}
