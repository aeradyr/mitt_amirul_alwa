CREATE DATABASE db_user_skill;

CREATE TABLE db_user_skill.dbo.tbl_t_user_skill (
	user_skill_id_pk nvarchar(50) PRIMARY KEY NOT NULL,
	user_name_fk nvarchar(50) NULL,
	skill_id_fk int NULL,
	skill_level_id_fk int NULL
);

CREATE TABLE db_user_skill.dbo.tbl_m_user (
	user_name_pk nvarchar(50) PRIMARY KEY NOT NULL,
	password nvarchar(50) NULL
);

CREATE TABLE db_user_skill.dbo.tbl_m_user_profile (
	user_name_fk nvarchar(50) PRIMARY KEY NOT NULL,
	name varchar(50) NULL,
	address	nvarchar(500),
	bod date,
	email nvarchar(50)
);

CREATE TABLE db_user_skill.dbo.tbl_m_skill_level(
	skill_level_id_pk int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	skill_level_name varchar(500) NULL,
);

CREATE TABLE db_user_skill.dbo.tbl_m_skill(
	skill_id_pk int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	skill_name varchar(500) NULL,
);